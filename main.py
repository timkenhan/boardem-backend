from datetime import datetime

from starlette.config import Config
from fastapi import FastAPI, Header, Request
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel, BaseSettings
from sqlmodel import Field, SQLModel, Session, create_engine, select
from sqlalchemy import func


class Settings(BaseSettings):
    app_name: str = 'BoardEm'
    cors_origins: list = []
    db_user: str
    db_pass: str
    db_name: str

    class Config:
        env_file = '.env'


app = FastAPI()
settings = Settings()
app.add_middleware(
    CORSMiddleware,
    allow_origins=settings.cors_origins,
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*'],
)
database_url = 'mysql+pymysql://%s:%s@localhost/%s' % (settings.db_user, settings.db_pass, settings.db_name)
engine = create_engine(database_url)


@app.get("/")
async def root():
    return {"message": "Hello World"}


class Message(SQLModel, table=True):
    id: int | None = Field(primary_key=True)
    name: str | None = Field(max_length=128)
    body: str
    source: str | None = Field(max_length=128)
    created: datetime = Field(default_factory=datetime.utcnow)


@app.on_event('startup')
async def startup():
    SQLModel.metadata.create_all(engine)



@app.get('/board')
async def list_entries(offset: int = 0, limit: int = 10):
    with Session(engine) as session:
        statement_entries = select(Message).order_by(Message.id.desc()).limit(limit).offset(offset)
        statement_count = select(func.count(Message.id))
        entries = session.exec(statement_entries).all()
        count = session.exec(statement_count).first()
    return dict(entries=entries, count=count)


@app.post('/board')
async def create_entry(request: Request, message: Message):
    source = request.client.host
    message.source = source
    with Session(engine) as session:
        session.add(message)
        session.commit()
        session.refresh(message)
    return message



operations_list = [ 'keygen', 'clear_db' ]
if __name__ == '__main__':
    from argparse import ArgumentParser
    from secrets import token_urlsafe
    parser = ArgumentParser()
    parser.add_argument('command', choices=operations_list, help='operations to do')
    args = parser.parse_args()
    if args.command == 'keygen':
        result = token_urlsafe()
        print(result)
    elif args.command == 'clear_db':
        # Message.__table__.drop(engine)
        SQLModel.metadata.drop_all(engine)
