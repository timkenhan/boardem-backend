## Setting up system

### System Packages

You'll need the following packages:
- python3-venv
- mariadb-server
- libmariadb-dev

These packages are on Debian/Ubuntu -- adapt on other system.

### Database

Create the database with:

```
CREATE USER 'user'@'localhost' IDENTIFIED BY 'password';
CREATE DATABASE dbname;
GRANT ALL PRIVILEGES ON dbname.* TO 'user'@'localhost';
```

Do replace `user`, `password`, and `dbname`.


## Setting up environment

Decide on a path to store all your files and ensure it exists.

This section assumes this code is already on that path and terminal pointing here.

### Virtual environment

Setup the Python3 virtual environment with the following commands:

```
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

### Environment variables

Copy the file `env-default` to `.env` and change the values to better fit your configuration.


## Deployment

There are two ways you can deploy this.

### Local Development

Simply run it with:

    venv/bin/uvicorn main:app

The application will be available from `http://127.0.0.1:8000`.

Add these options when running it from outside of source dir:

    --app-dir=path/to/backend --env-file=path/to/backend/.env

Host address and port number can be set with `--host` and `--port` options respectively.

### Production

To do...